/* use strict */
var INFO = ["plugin", { name: "content-length", version: "0.1",
    href: "http://code.google.com/p/dactyl/issues/list",
    summary: "HTTP content length retriever",
    xmlns: "dactyl" },
    ["author", { email: "rafael.f.fernandez@gmail.com" }, "Rafael Fernandez"],
    ["license", { href: "http://opensource.org/licenses/mit-license.php" }, "MIT"],
    ["project", { name: "Pentadactyl", "min-version": "1.0" }],
    ["p", {}, "This plugins retrieves the content length of a hint"],
    ["item", {},
        ["tags", {}, ";l"],
        ["strut"],
        ["spec", {}, ";l"],
        ["description", {},
            ["p", {},
                "Retrieves the content length of a hint by sending a HEAD ",
                "request and parsing the content-length field of the response."]]]];

function convertBytes(bytes) {

    var units = 'BKMG';
    var idx = 0;

    if (bytes > 1073741824) {
        bytes /= 1073741824;
        idx = 3;
    } else if (bytes > 1048576) {
        bytes /= 1048576;
        idx = 2;
    } else if (bytes > 1024) {
        bytes /= 1024;
        idx = 1;
    }

    return bytes.toFixed(2) + '' + units.charAt(idx);
}

hints.addMode('l', "Retrieve content length of hint", function(elem) {

    var xhr = new XMLHttpRequest();

    xhr.open("HEAD", elem, true);

    xhr.onreadystatechange = function() {

        if (xhr.readyState != 4) {
            // operation is not complete yet
            return;
        }

        if (xhr.status != 200) {
            // request was not successful
            dactyl.echo(xhr.statusText);
            return;
        }

        var len = parseInt(xhr.getResponseHeader('Content-Length'));

        if (isNaN(len)) {
            dactyl.echo("content-length not supported");
        } else {
            dactyl.echo(len + "b" + " (~" + convertBytes(len) + ")");
        }
    };

    xhr.send(null);
});

/* vim:se sts=4 sw=4 et: */
