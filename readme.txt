Set up on Windows

1. Create symlink ~/_pentadactylrc -> veeven-config-files/pentadactyl/pentadactyl-win/_pentadactylrc

2. Create ~/pentadactyl folder

3. Create symlinks
	* [/D] ~/pentadactyl/colors  -> veeven-config-files/pentadactyl/shared/colors
	* [/D] ~/pentadactyl/plugins -> veeven-config-files/pentadactyl/shared/plugins
	* ~/pentadactyl/abbreviations -> veeven-config-files/pentadactyl/shared/abbreviations
